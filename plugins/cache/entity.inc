<?php

/**
 * @file
 * Provides a simple time-based caching option for panel panes.
 */

// Plugin definition
$plugin = array(
  'title' => t('Entity cache'),
  'description' => t('Caches the entities in the current context.'),
  'cache get' => 'panels_entity_cache_get_cache',
  'cache set' => 'panels_entity_cache_set_cache',
  'cache clear' => 'panels_entity_cache_clear_cache',
  'settings form' => 'panels_entity_cache_settings_form',
  'settings form submit' => 'panels_entity_cache_settings_form_submit',
);

/**
 * Get cached content.
 */
function panels_entity_cache_get_cache($conf, $display, $args, $contexts, $pane = NULL) {
  $cids = panels_entity_cache_get_ids($conf, $display, $args, $contexts, $pane);

  foreach ($cids as $cid) {
    $cache = cache_get($cid, 'cache');
  }

  if (!$cache) {
    return FALSE;
  }

  return $cache->data;
}

/**
 * Set cached content.
 */
function panels_entity_cache_set_cache($conf, $content, $display, $args, $contexts, $pane = NULL) {
  $cids = panels_entity_cache_get_ids($conf, $display, $args, $contexts, $pane);
  foreach ($cids as $cid) {
    cache_set($cid, $content);
  }
}

/**
 * Clear cached content.
 *
 * Cache clears are always for an entire display, regardless of arguments.
 */
function panels_entity_cache_clear_cache($display) {
  $cid = 'panels_entity_cache';

  // This is used in case this is an in-code display, which means did will be something like 'new-1'.
  if (isset($display->owner) && isset($display->owner->id)) {
    $cid .= ':' . $display->owner->id;
  }
  $cid .= ':' . $display->did;

  cache_clear_all($cid, 'cache', TRUE);
}

/**
 * Figure out an id for our cache based upon input and settings.
 */
function panels_entity_cache_get_ids($conf, $display, $args, $contexts, $pane) {

  $pane_info = '';

  // If the panel is stored in the database it'll have a numeric did value.
  if (is_numeric($display->did)) {
    $pane_info .= ':' . $display->did;
  }
  // Exported panels won't have a numeric did but may have a usable cache_key.
  elseif (!empty($display->cache_key)) {
    $pane_info .= ':' . str_replace('panel_context:', '', $display->cache_key);
  }
  // Alternatively use the css_id.
  elseif (!empty($display->css_id)) {
    $pane_info .= ':' . $display->css_id;
  }
  // Failover to just appending the did, which may be the completely unusable
  // string 'new'.
  else {
    $pane_info .= ':' . $display->did;
  }

  if ($pane) {
    $pane_info .= ':' . $pane->pid;
  }

  if (user_access('view pane admin links')) {
    $pane_info .= ':admin';
  }

  if (module_exists('locale')) {
    global $language;
    $pane_info .= ':' . $language->language;
  }

  if(!empty($pane->configuration['use_pager']) && !empty($_GET['page'])) {
    $pane_info .= ':p' . check_plain($_GET['page']);
  }

  foreach ($contexts as $context) {
    $ids[] = 'panels_entity_cache__' . $context->type['0'] . '__' . $context->argument . '__' . $pane_info;
  }

  return $ids;
}

function panels_entity_cache_settings_form($conf, $display, $pid) {
  return $form;
}
